﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace STM32sPLOT
{
    public partial class Form1 : Form
    {
        int Mode = 0; // Гироскоп - 0, Аксель - 1, Компас - 2
        bool start = true;
        bool dateTypeKvatro = false;
        private const double dt = 0.05;
        private double dtYHHHHHH = (DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;
        private double x = 0;
        private double y = 0;
        private string command = "";

        SerialPort _serialPort;
        
        public Form1()
        {
            InitializeComponent();
            try
            {
                button4.Enabled = false;
                button4.Text = "GYRO ON";
                chart1.Series[0].LegendText = "X";
                chart2.Series[0].LegendText = "Y";
                chart3.Series[0].LegendText = "Z";
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void preparingDate()
        {
            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();
            chart3.Series[0].Points.Clear();
            _serialPort = new SerialPort("COM5",
                                       115200,
                                       Parity.None,
                                       8,
                                       StopBits.One);
            _serialPort.Handshake = Handshake.None;
            _serialPort.DtrEnable = true;
            try
            {
                _serialPort.Open();
            }
            catch (Exception eSerialPort)
            {
                MessageBox.Show(eSerialPort.Message);

                return;
            }

            byte[] newByte = new byte[20];
            _serialPort.Read(newByte, 0, 20);

            double[] massX = new double[30];
            double[] massY = new double[30];
            double[] massZ = new double[30];
            while (start)
            {

                switch (Mode)
                {
                    case 0: // Гироскоп
                        for (int i = 0; i < 30; i++)
                        {
                            try
                            {
                                command = BitConverter.ToString(newByte);

                            }
                            catch (Exception exp)
                            {
                                MessageBox.Show(exp.Message);
                            }

                            massX[i] = ((newByte[2] << 8) | newByte[3]) / 131.0;
                            massY[i] = ((newByte[4] << 8) | newByte[5]) / 131.0;
                            massZ[i] = ((newByte[6] << 8) | newByte[7]) / 131.0;

                            _serialPort.Read(newByte, 0, 20);
                        }
                        break;
                    case 1: // Акселерометр
                        for (int i = 0; i < 30; i++)
                        {
                            try
                            {
                                command = System.Text.Encoding.Default.GetString(newByte);

                            }
                            catch (Exception exp) { MessageBox.Show(exp.Message); }

                            massX[i] = ((newByte[8] << 8) | newByte[9]) / 2048.0;
                            massY[i] = ((newByte[10] << 8) | newByte[11]) / 2048.0;
                            massZ[i] = ((newByte[12] << 8) | newByte[13]) / 2048.0;

                            _serialPort.Read(newByte, 0, 20);
                        }

                        break;
                    case 2: // Компасс
                        for (int i = 0; i < 30; i++)
                        {
                            try
                            {
                                command = System.Text.Encoding.Default.GetString(newByte);

                            }
                            catch (Exception exp) { MessageBox.Show(exp.Message); }

                            massX[i] = ((newByte[14] << 8) | newByte[15]) / 131.0;
                            massY[i] = ((newByte[16] << 8) | newByte[17]) / 131.0;
                            massZ[i] = ((newByte[18] << 8) | newByte[19]) / 131.0;

                            _serialPort.Read(newByte, 0, 20);
                        }

                        break;
                    default:
                        break;
                }
                Application.DoEvents();
                drawGraph(massX, massY, massZ);
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
             preparingDate();

        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            MessageBox.Show("");
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }


        private void drawGraph(double[] massX, double[] massY, double[] massZ)
        {
            massX = BubbleSort(massX);
            massY = BubbleSort(massY);
            massZ = BubbleSort(massZ);

            y = averageArray(massX);
            if(Mode == 0) y = 499 - y;
            listBox1.Items.Add(y);
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
            chart1.Series[0].Points.AddXY(x, y);

            y = averageArray(massY);
            if (Mode == 0) y = 499 - y;
            listBox2.Items.Add(y);
            listBox2.SelectedIndex = listBox2.Items.Count - 1;
            chart2.Series[0].Points.AddXY(x, y);

            y = averageArray(massZ);
            if (Mode == 0) y = 499 - y;
            else if (Mode == 1) y = 17 - y;
            listBox3.Items.Add(y);
            listBox3.SelectedIndex = listBox1.Items.Count - 1;
            chart3.Series[0].Points.AddXY(x, y);

            x = x + 30 * dt;
            Application.DoEvents();
        }

        private double averageArray(double[] mass)
        {
            double y = 0;
            for (int i = 5; i < 23; i++)
            {
                y = y + mass[i];
            }
            y = y / 18;
            y = Math.Truncate(y);
            return y;
        }

        private double[] BubbleSort(double[] A)
        {
            for (int i = 0; i < A.Length; i++)
            {
                for (int j = 0; j < A.Length - i - 1; j++)
                {
                    if (A[j] > A[j + 1])
                    {
                        double temp = A[j];
                        A[j] = A[j + 1];
                        A[j + 1] = temp;
                    }
                }
            }
            return A;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            start = false;
            _serialPort.Close();
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        // Гироскоп - вкл
        private void button4_Click(object sender, EventArgs e)
        {
            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();
            chart3.Series[0].Points.Clear();
            if (Mode != 0)
            {
                button6.Text = "COMP";
                button5.Text = "ACCEL";
                button4.Text = "GYRO ON";
            }
            Mode = 0;

            chart1.Series[0].LegendText = "XGyro";
            chart2.Series[0].LegendText = "YGyro";
            chart3.Series[0].LegendText = "ZGyro";
            button4.Enabled = false;
            button5.Enabled = true;
            button6.Enabled = true;
        }

        // Аксель - вкл
        private void button5_Click(object sender, EventArgs e)
        {
            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();
            chart3.Series[0].Points.Clear();
            if (Mode != 1)
            {
                button6.Text = "COMP";
                button5.Text = "ACCEL ON";
                button4.Text = "GYRO";
            }

            Mode = 1;

            chart1.Series[0].LegendText = "XAccel";
            chart2.Series[0].LegendText = "YAccel";
            chart3.Series[0].LegendText = "ZAccel";
            button4.Enabled = true;
            button5.Enabled = false;
            button6.Enabled = true;
        }

        // Компас - вкл
        private void button6_Click(object sender, EventArgs e)
        {
            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();
            chart3.Series[0].Points.Clear();
            if (Mode != 2)
            {
                button6.Text = "COMP ON";
                button5.Text = "ACCEL";
                button4.Text = "GYRO";
            }

            Mode = 2;
            chart1.Series[0].LegendText = "XComp";
            chart2.Series[0].LegendText = "YComp";
            chart3.Series[0].LegendText = "ZComp";
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = false;
            
        }

    }
}
